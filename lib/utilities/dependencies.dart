import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_bloc.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_bloc.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_bloc.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/auth_repo.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/login_repo.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/verify_email_repo.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/local/shared_preference/shared_preference_helper.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/local/sqlite_database/sql_database.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/dio/network_dio_client.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/dio/network_module.dart';
import 'package:x_circle_web/constants/app_theme.dart';

class Dependencies {
  static Future<void> inject() async {
    final getIt = GetIt.I;

    //INJECT THEME
    getIt.registerSingleton<AppTheme>(AppTheme(isLightTheme: true));

    //     //Navigation
    // getIt.registerLazySingleton<NavigationService>(() => NavigationService());

    // //Route Observer
    // getIt.registerLazySingleton<RouteObserver<ModalRoute>>(
    //     () => RouteObserver<ModalRoute>());

    //BlOC DEPENDENCIES:----------------------------------------------------------

    //****** DATA **********************

    //Shared
    getIt.registerSingletonAsync<SharedPreferences>(
        () => SharedPreferences.getInstance());
    getIt.registerSingleton(
        SharedPreferenceHelper(await getIt.getAsync<SharedPreferences>()));

    //Database
    getIt.registerSingletonAsync<Database>(() => SqlDatabase.database());

    //REMOTE
    getIt.registerSingleton<Dio>(
        NetworkModule.provideDio(getIt<SharedPreferenceHelper>()));
    getIt.registerSingleton(NetworkClientDio(getIt<Dio>()));

    //************************************/

    //****** REPOSITORY **********************
    getIt.registerSingleton(AuthRepository(getIt<SharedPreferenceHelper>()));
    getIt.registerSingleton(LoginRepository(getIt<NetworkClientDio>()));
    getIt.registerSingleton(VerifyEmailRepository(getIt<NetworkClientDio>()));
    //************************************/

    //****** BLOCS **********************
    getIt.registerSingleton(AuthBloc(authRepository: getIt<AuthRepository>()));
    getIt.registerSingleton(
        LoginBloc(loginRepository: getIt<LoginRepository>()));
    getIt.registerSingleton(
        VerifyEmailBloc(verifyRepository: getIt<VerifyEmailRepository>()));
    //************************************/

    await getIt.allReady();
  }
}
