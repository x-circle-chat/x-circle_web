extension PointSize on double {
  double toPoints() => this * 0.75;
}
