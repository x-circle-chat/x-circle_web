import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_bloc.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_event.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_state.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_bloc.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_bloc.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/auth_repo.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/login_repo.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/verify_email_repo.dart';
import 'package:x_circle_web/constants/routes.dart';
import 'package:x_circle_web/utilities/dependencies.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //Inject Dependencies
  await Dependencies.inject();

  //Run App
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) {
            final authBloc = AuthBloc(
              authRepository: GetIt.I<AuthRepository>(),
            );
            authBloc.add(Initialize());
            return authBloc;
          },
        ),
        BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(
            loginRepository: GetIt.I<LoginRepository>(),
          ),
        ),
        BlocProvider<VerifyEmailBloc>(
          create: (context) => VerifyEmailBloc(
            verifyRepository: GetIt.I<VerifyEmailRepository>(),
          ),
        )
      ],
      child: BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {},
        builder: (context, AuthState state) {
          return createMaterialApp(state.status);
        },
      ),
    );
  }

  //METHODS
  Widget createMaterialApp(AuthStatus authStatus) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      routerConfig: routerConfig(authStatus),
    );
  }
}
