// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:lottie/lottie.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';

Widget ActivityView({required bool loading, required Widget child}) {
  return SizedBox(
    width: double.infinity,
    height: double.infinity,
    child: Stack(
      alignment: Alignment.center,
      children: [
        child,
        if (loading)
          Container(
            width: double.infinity,
            height: double.infinity,
            color: GetIt.I<AppTheme>().borderGrayColor.withOpacity(0.8),
          ),
        if (loading)
          Container(
            width: 100,
            height: 100,
            padding: const EdgeInsets.all(0.0),
            decoration: BoxDecoration(
              color: GetIt.I<AppTheme>().themeColor.withOpacity(0.9),
              borderRadius: BorderRadius.circular(16),
            ),
            child: Lottie.asset(
              LottiePaths.loader1,
              animate: true,
              repeat: true,
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.cover,
            ),
          )
      ],
    ),
  );
}
