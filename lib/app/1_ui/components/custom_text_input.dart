import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';

Widget customTextInput(
    {required String placeholder,
    required TextEditingController controller,
    Function(String)? onChanged}) {
  return Container(
    height: 52,
    padding: const EdgeInsets.symmetric(horizontal: 20),
    decoration: BoxDecoration(
      color: GetIt.I<AppTheme>().cardBackgroundColor,
      border: Border.all(width: 2, color: GetIt.I<AppTheme>().themeColor),
      borderRadius: BorderRadius.circular(26),
    ),
    child: TextField(
      controller: controller,
      onChanged: onChanged,
      style: TextStyle(
        color: GetIt.I<AppTheme>().primaryTextColor,
        fontFamily: AppFont.poppins,
        fontWeight: FontWeight.w500,
        fontSize: 16,
      ),
      cursorColor: GetIt.I<AppTheme>().primaryTextColor,
      cursorHeight: 16,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: placeholder,
        hintStyle: TextStyle(
          color: GetIt.I<AppTheme>().lightTextColor,
          fontFamily: AppFont.poppins,
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
      ),
    ),
  );
}
