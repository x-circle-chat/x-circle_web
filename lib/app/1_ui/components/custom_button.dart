import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';

Widget customButton(
    {required String title,
    double fontSize = 22,
    required void Function() action}) {
  return InkWell(
    onTap: action,
    child: Container(
      height: 52,
      decoration: BoxDecoration(
        color: GetIt.I<AppTheme>().themeColor,
        borderRadius: BorderRadius.circular(26),
      ),
      child: Center(
        child: Text(
          title,
          style: TextStyle(
            color: GetIt.I<AppTheme>().blackColor,
            fontFamily: AppFont.poppins,
            fontWeight: FontWeight.w600,
            fontSize: fontSize,
          ),
        ),
      ),
    ),
  );
}
