import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';

Widget searchTextInput({required String placeholder}) {
  return Container(
    height: 36,
    padding: const EdgeInsets.symmetric(horizontal: 16),
    decoration: BoxDecoration(
      color: GetIt.I<AppTheme>().inputBGColor,
      border: Border.all(width: 1, color: GetIt.I<AppTheme>().inputBorderColor),
      borderRadius: BorderRadius.circular(26),
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          AssetPath.search,
          width: 18,
          height: 18,
        ),
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: TextField(
            style: TextStyle(
              color: GetIt.I<AppTheme>().primaryTextColor,
              fontFamily: AppFont.poppins,
              fontWeight: FontWeight.w500,
              fontSize: 14,
            ),
            cursorColor: GetIt.I<AppTheme>().primaryTextColor,
            cursorHeight: 14,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
              isDense: true,
              border: InputBorder.none,
              hintText: placeholder,
              hintStyle: TextStyle(
                color: GetIt.I<AppTheme>().lightTextColor,
                fontFamily: AppFont.poppins,
                fontWeight: FontWeight.w500,
                fontSize: 14,
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 16,
        ),
        Image.asset(
          AssetPath.record,
          width: 18,
          height: 18,
        ),
      ],
    ),
  );
}
