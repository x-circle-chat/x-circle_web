// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';

Widget ErrorView(
    {required bool visible,
    required Widget child,
    required String errorMessage,
    required Function() onTap}) {
  return SizedBox(
    width: double.infinity,
    height: double.infinity,
    child: Stack(
      alignment: Alignment.center,
      children: [
        child,
        if (visible)
          Container(
            width: double.infinity,
            height: double.infinity,
            color: GetIt.I<AppTheme>().borderGrayColor.withOpacity(0.8),
          ),
        if (visible)
          Container(
            margin: const EdgeInsets.symmetric(vertical: 40, horizontal: 100),
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            decoration: BoxDecoration(
              color: GetIt.I<AppTheme>().blackColor,
              borderRadius: BorderRadius.circular(16),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  Strings.error,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: GetIt.I<AppTheme>().whiteColor,
                    fontFamily: AppFont.poppins,
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  errorMessage,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: GetIt.I<AppTheme>().whiteColor,
                    fontFamily: AppFont.poppins,
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                InkWell(
                  onTap: onTap,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: GetIt.I<AppTheme>().whiteColor,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      Strings.okay,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: GetIt.I<AppTheme>().primaryTextColor,
                        fontFamily: AppFont.poppins,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
      ],
    ),
  );
}
