import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';

Widget otpTextInput(
    {required String placeholder,
    required Function(String) onCodeChanged,
    required Function(String) onSubmit}) {
  return SizedBox(
    height: 52,
    child: OtpTextField(
      fieldWidth: 52,
      numberOfFields: 6,
      textStyle: TextStyle(
        color: GetIt.I<AppTheme>().primaryTextColor,
        fontFamily: AppFont.poppins,
        fontWeight: FontWeight.w500,
        fontSize: 22,
      ),
      margin: const EdgeInsets.fromLTRB(0, 0, 16, 0),
      cursorColor: GetIt.I<AppTheme>().primaryTextColor,
      crossAxisAlignment: CrossAxisAlignment.center,
      borderColor: GetIt.I<AppTheme>().themeColor,
      enabledBorderColor: GetIt.I<AppTheme>().themeColor,
      focusedBorderColor: GetIt.I<AppTheme>().themeColor,

      //set to true to show as box or false to show as dash
      showFieldAsBox: false,
      //runs when a code is typed in
      onCodeChanged: onCodeChanged,
      //runs when every textfield is filled
      onSubmit: onSubmit, // end onSubmit
    ),
  );
}
