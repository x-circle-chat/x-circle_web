import 'package:flutter/material.dart';
import 'package:x_circle_web/app/1_ui/models/menu_model.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/home.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/2.explore/explore.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/3.feed/feed.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/4.settings/settings.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/5.contact/contact.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/strings.dart';

enum MenuOptions { home, explore, feed, settings, contact }

extension CurrentData on MenuOptions {
  static List<MenuOptions> allOptions() {
    return [
      MenuOptions.home,
      MenuOptions.explore,
      MenuOptions.feed,
      MenuOptions.settings,
      MenuOptions.contact,
    ];
  }

  MenuModel optionModel() {
    switch (this) {
      case MenuOptions.home:
        return MenuModel(
          title: Strings.home,
          icon: AssetPath.homeLogo,
        );
      case MenuOptions.explore:
        return MenuModel(
          title: Strings.explore,
          icon: AssetPath.exploreLogo,
        );
      case MenuOptions.feed:
        return MenuModel(
          title: Strings.feed,
          icon: AssetPath.feedLogo,
        );
      case MenuOptions.settings:
        return MenuModel(
          title: Strings.settings,
          icon: AssetPath.settingsLogo,
        );
      case MenuOptions.contact:
        return MenuModel(
          title: Strings.contact,
          icon: AssetPath.contactLogo,
        );
      default:
        return MenuModel(
          title: Strings.home,
          icon: AssetPath.homeLogo,
        );
    }
  }

  Widget containerWidget() {
    switch (this) {
      case MenuOptions.home:
        return const Home();
      case MenuOptions.explore:
        return const Explore();
      case MenuOptions.feed:
        return const Feed();
      case MenuOptions.settings:
        return const Settings();
      case MenuOptions.contact:
        return const Contact();
      default:
        return const Home();
    }
  }
}
