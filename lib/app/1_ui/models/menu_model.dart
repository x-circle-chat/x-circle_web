class MenuModel {
  String title;
  String icon;

  MenuModel({
    required this.title,
    required this.icon,
  });
}
