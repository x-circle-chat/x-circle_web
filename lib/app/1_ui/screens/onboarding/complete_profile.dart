
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:x_circle_web/app/1_ui/components/activity_view.dart';
import 'package:x_circle_web/app/1_ui/components/custom_button.dart';
import 'package:x_circle_web/app/1_ui/components/custom_text_input.dart';
import 'package:x_circle_web/app/1_ui/components/error_view.dart';
import 'package:x_circle_web/app/1_ui/screens/onboarding/login/verify_email.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_bloc.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_event.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_state.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';

class CompleteProfile extends StatelessWidget {
  static const route = "complete_profile";
  CompleteProfile({super.key});

  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    LoginBloc loginBloc = BlocProvider.of<LoginBloc>(context);
    return Scaffold(
        backgroundColor: GetIt.I<AppTheme>().backgroundColor,
        body: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state.status is FormValidated) {
              loginBloc.add(LoginUser());
            } else if (state.status is Success) {
              context.goNamed(
                VerifyEmail.route,
                extra: {
                  "otpId": (state.status as Success).verificationKey.otpId,
                  "email": state.email
                },
              );
            }
          },
          builder: (context, LoginState state) {
            if (state.status is Initial) {
              return _body(context);
            } else if (state.status is FormValidated) {
              return _body(context);
            } else if (state.status is Loading) {
              return ActivityView(loading: true, child: _body(context));
            } else if (state.status is Error) {
              return ErrorView(
                  onTap: () {
                    loginBloc.add(DismissError(
                        errorMessage: (state.status as Error).errorMessage));
                  },
                  visible: (state.status as Error).visible,
                  child: _body(context),
                  errorMessage: (state.status as Error).errorMessage);
            } else {
              return _body(context);
            }
          },
        )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  //============= WIDGETS ==================

  //BODY
  Widget _body(BuildContext context) {
    //Execute Code
    LoginBloc loginBloc = BlocProvider.of<LoginBloc>(context);
    controller.text = loginBloc.state.email;

    //Return Widget
    return Center(
      child: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          child: Container(
            width: 500,
            padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 32),
            decoration: BoxDecoration(
                color: GetIt.I<AppTheme>().cardBackgroundColor,
                borderRadius: BorderRadius.circular(16)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  AssetPath.appLogo,
                  width: 100,
                  height: 100,
                ),
                const SizedBox(
                  height: 64,
                ),
                Row(
                  children: [
                    Text(
                      Strings.completeYourProfile,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: GetIt.I<AppTheme>().lightTextColor,
                        fontFamily: AppFont.poppins,
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 24,
                ),
                customTextInput(
                  placeholder: Strings.enterName,
                  controller: controller,
                  onChanged: (value) {
                    loginBloc.add(UpdateEmail(email: value));
                  },
                ),
                const SizedBox(
                  height: 24,
                ),
                customTextInput(
                  placeholder: Strings.enterBirthday,
                  controller: controller,
                  onChanged: (value) {
                    loginBloc.add(UpdateEmail(email: value));
                  },
                ),
                const SizedBox(
                  height: 24,
                ),
                customTextInput(
                  placeholder: Strings.selGender,
                  controller: controller,
                  onChanged: (value) {
                    loginBloc.add(UpdateEmail(email: value));
                  },
                ),
                const SizedBox(
                  height: 48,
                ),
                customButton(
                  title: Strings.submitButton,
                  action: () {
                    loginBloc.add(ValidateForm());
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //============= METHODS ==================

  //============= ACTIONS ==================
}
