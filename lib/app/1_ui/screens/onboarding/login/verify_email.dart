import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/components/activity_view.dart';
import 'package:x_circle_web/app/1_ui/components/custom_button.dart';
import 'package:x_circle_web/app/1_ui/components/error_view.dart';
import 'package:x_circle_web/app/1_ui/components/otp_text_input.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_bloc.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_event.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_bloc.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_event.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_state.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';

class VerifyEmail extends StatelessWidget {
  static const route = "verify_email";
  final String otpId;
  final String email;
  VerifyEmail({super.key, required this.otpId, required this.email});

  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    VerifyEmailBloc verifyEmailBloc = BlocProvider.of<VerifyEmailBloc>(context);

    return Scaffold(
      backgroundColor: GetIt.I<AppTheme>().backgroundColor,
      body: BlocConsumer<VerifyEmailBloc, VerifyEmailState>(
        listener: (context, state) {
          if (state.status is FormValidated) {
            verifyEmailBloc.add(VerifyUser(otpId: otpId));
          } else if (state.status is Success) {
            AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
            authBloc.add(Login(authModel: (state.status as Success).authModel));
          }
        },
        builder: (context, VerifyEmailState state) {
          if (state.status is Initial) {
            return _body(context);
          } else if (state.status is FormValidated) {
            return _body(context);
          } else if (state.status is Loading) {
            return ActivityView(loading: true, child: _body(context));
          } else if (state.status is Error) {
            return ErrorView(
                onTap: () {
                  verifyEmailBloc.add(DismissError(
                      errorMessage: (state.status as Error).errorMessage));
                },
                visible: (state.status as Error).visible,
                child: _body(context),
                errorMessage: (state.status as Error).errorMessage);
          } else {
            return _body(context);
          }
        },
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  //============= WIDGETS ==================

  //BODY
  Widget _body(BuildContext context) {
    //Execute Code
    VerifyEmailBloc verifyEmailBloc = BlocProvider.of<VerifyEmailBloc>(context);
    controller.text = verifyEmailBloc.state.otp;

    //Return Widget
    return Center(
      child: Container(
        width: 500,
        padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 32),
        decoration: BoxDecoration(
            color: GetIt.I<AppTheme>().cardBackgroundColor,
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              AssetPath.appLogo,
              width: 100,
              height: 100,
            ),
            const SizedBox(
              height: 64,
            ),
            Row(
              children: [
                Text(
                  "${Strings.enterOTPHeadline}$email",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: GetIt.I<AppTheme>().lightTextColor,
                    fontFamily: AppFont.poppins,
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 24,
            ),
            otpTextInput(
              placeholder: Strings.loginButton,
              onCodeChanged: (otp) {
                verifyEmailBloc.add(UpdateOtp(otp: otp));
              },
              onSubmit: (verificationCode) {
                verifyEmailBloc.add(UpdateOtp(otp: verificationCode));
              },
            ),
            const SizedBox(
              height: 48,
            ),
            customButton(
              title: Strings.submitButton,
              action: () {
                verifyEmailBloc.add(ValidateForm());
              },
            ),
          ],
        ),
      ),
    );
  }

  //============= METHODS ==================

  //============= ACTIONS ==================
}
