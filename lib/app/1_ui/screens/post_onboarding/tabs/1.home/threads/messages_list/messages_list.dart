import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/threads/messages_list/message_tile.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class MessagesList extends StatelessWidget {
  const MessagesList({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      scrollDirection: Axis.vertical,
      slivers: [
        listSection(),
        listSection(),
        listSection(),
      ],
    );
  }

  Widget listSection() {
    return SliverStickyHeader(
      header: Container(
        height: 60,
        color: GetIt.I<AppTheme>().backgroundColor,
        alignment: Alignment.centerLeft,
        child: Text(
          "RECENT CHAT",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: GetIt.I<AppTheme>().blackColor,
            fontFamily: AppFont.poppins,
            fontWeight: FontWeight.w500,
            fontSize: 14.0.toPoints(),
          ),
        ),
      ),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, i) => const MessagesTile(),
          childCount: 5,
        ),
      ),
    );
  }
}
