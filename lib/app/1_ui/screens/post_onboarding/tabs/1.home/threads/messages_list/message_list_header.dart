import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class MessagesListHeader extends StatelessWidget {
  const MessagesListHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      color: GetIt.I<AppTheme>().backgroundColor,
      child: Row(
        children: [
          Text(
            Strings.message,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: GetIt.I<AppTheme>().blackColor,
              fontFamily: AppFont.poppins,
              fontWeight: FontWeight.w500,
              fontSize: 19.5.toPoints(),
            ),
          ),
          const Spacer(),
          InkWell(
            child: Container(
              height: 30,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 12),
              decoration: BoxDecoration(
                color: GetIt.I<AppTheme>().themeColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                  width: 1,
                  color: GetIt.I<AppTheme>().themeColor,
                ),
              ),
              child: Text(
                Strings.archive,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: GetIt.I<AppTheme>().blackColor,
                  fontFamily: AppFont.poppins,
                  fontWeight: FontWeight.w500,
                  fontSize: 12.0.toPoints(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
