import 'package:flutter/material.dart';
import 'package:x_circle_web/constants/asset_path.dart';

class StoriesList extends StatelessWidget {
  StoriesList({super.key});

  final double gap = 8.0;
  final List<String> stories = [
    AssetPath.add,
    AssetPath.story1,
    AssetPath.story2,
    AssetPath.story3,
    AssetPath.story4,
    AssetPath.story5,
  ];

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => SizedBox(
        width: double.infinity,
        height: tileWidth(constraints),
        child: ListView.builder(
          itemCount: stories.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, index) {
            return Padding(
              padding: EdgeInsets.only(left: index == 0 ? 0 : gap),
              child: Container(
                height: tileWidth(constraints),
                width: tileWidth(constraints),
                padding: EdgeInsets.all(index == 0 ? 0 : 3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    tileWidth(constraints) / 2,
                  ),
                  border: Border.all(
                    width: index == 0 ? 0 : 1,
                    color: index == 0 ? Colors.transparent : Colors.blue,
                  ),
                ),
                child: Image.asset(
                  stories[index],
                  width: double.infinity,
                  height: double.infinity,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  double tileWidth(BoxConstraints constraints) {
    return (constraints.maxWidth - (5 * gap)) / 6;
  }
}
