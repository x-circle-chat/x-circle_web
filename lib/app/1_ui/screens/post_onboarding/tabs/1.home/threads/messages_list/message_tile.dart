import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class MessagesTile extends StatelessWidget {
  final isOnline = true;
  const MessagesTile({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: double.infinity,
      margin: const EdgeInsets.only(bottom: 24),
      color: GetIt.I<AppTheme>().backgroundColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 42,
            width: 42,
            padding: EdgeInsets.all(isOnline ? 3 : 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              border: Border.all(
                width: isOnline ? 1 : 0,
                color: isOnline ? Colors.blue : Colors.transparent,
              ),
            ),
            child: Image.asset(
              AssetPath.story4,
              width: double.infinity,
              height: double.infinity,
            ),
          ),
          const SizedBox(
            width: 16,
            height: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                LayoutBuilder(builder: (context, constraints) {
                  return Row(
                    children: [
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            maxWidth: constraints.maxWidth * 0.7),
                        child: Text(
                          "Smith Mathew",
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          style: TextStyle(
                            color: GetIt.I<AppTheme>().blackColor,
                            fontFamily: AppFont.poppins,
                            fontWeight: FontWeight.w500,
                            fontSize: 19.5.toPoints(),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: GetIt.I<AppTheme>().themeColor,
                        ),
                        child: Text(
                          "2",
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          style: TextStyle(
                            color: GetIt.I<AppTheme>().blackColor,
                            fontFamily: AppFont.poppins,
                            fontWeight: FontWeight.w700,
                            fontSize: 10.0.toPoints(),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text(
                          "29 march",
                          textAlign: TextAlign.right,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: GetIt.I<AppTheme>().subtitleTextColor,
                            fontFamily: AppFont.poppins,
                            fontWeight: FontWeight.w400,
                            fontSize: 16.0.toPoints(),
                          ),
                        ),
                      )
                    ],
                  );
                }),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  "Are you ready for today’s part to take part.",
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: GetIt.I<AppTheme>().subtitleTextColor,
                    fontFamily: AppFont.poppins,
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0.toPoints(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
