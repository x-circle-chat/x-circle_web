import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/components/search_text_input.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/threads/messages_list/message_list_header.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/threads/messages_list/messages_list.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/threads/stories_list/stories_list.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class Threads extends StatelessWidget {
  const Threads({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 450.0.toPoints(),
      height: double.infinity,
      color: Colors.transparent,
      padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 25),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                Strings.chat,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: GetIt.I<AppTheme>().blackColor,
                  fontFamily: AppFont.poppins,
                  fontWeight: FontWeight.w500,
                  fontSize: 19.5.toPoints(),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 16,
            width: 16,
          ),
          searchTextInput(placeholder: Strings.searchHere),
          const SizedBox(
            height: 35,
            width: 16,
          ),
          StoriesList(),
          const SizedBox(
            height: 32,
            width: 16,
          ),
          const MessagesListHeader(),
          const SizedBox(
            height: 4,
            width: 12,
          ),
          const Expanded(
            child: MessagesList(),
          )
        ],
      ),
    );
  }
}
