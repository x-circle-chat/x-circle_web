import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/chat/chat-bubbles/text_bubble/text_bubble.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/constants/strings.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class Chat extends StatelessWidget {
  const Chat({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        chatHeader(),
        Container(
          height: 1,
          width: double.infinity,
          color: GetIt.I<AppTheme>().borderColor,
        ),
        const SizedBox(
          height: 32,
          width: 16,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: messagesListView(),
          ),
        ),
        Container(
          height: 1,
          width: double.infinity,
          color: GetIt.I<AppTheme>().borderColor,
        ),
        chatInputView(),
      ],
    );
  }

//======================= HEADER SECTION ==================
  Widget chatHeader() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                Image.asset(
                  AssetPath.story4,
                  width: 42,
                  height: 42,
                ),
                const SizedBox(
                  width: 16,
                  height: 16,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Smith Mathew",
                        textAlign: TextAlign.left,
                        maxLines: 1,
                        style: TextStyle(
                          color: GetIt.I<AppTheme>().blackColor,
                          fontFamily: AppFont.poppins,
                          fontWeight: FontWeight.w500,
                          fontSize: 19.5.toPoints(),
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Row(
                        children: [
                          Text(
                            "Active Now",
                            textAlign: TextAlign.right,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: GetIt.I<AppTheme>().subtitleTextColor,
                              fontFamily: AppFont.poppins,
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0.toPoints(),
                            ),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                              color: GetIt.I<AppTheme>().themeColor,
                              borderRadius: BorderRadius.circular(4),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            width: 32,
          ),
          Row(
            children: [
              Image.asset(
                AssetPath.phone,
                width: 20,
                height: 20,
              ),
              const SizedBox(
                width: 16,
              ),
              Image.asset(
                AssetPath.camera,
                width: 20,
                height: 20,
              ),
            ],
          )
        ],
      ),
    );
  }

//======================= CHAT MESSAGES SECTION ==================
  Widget messagesListView() {
    return CustomScrollView(
      scrollDirection: Axis.vertical,
      slivers: [
        listSection(),
        listSection(),
        listSection(),
      ],
    );
  }

  Widget listSection() {
    return SliverStickyHeader(
      header: Container(
        height: 60,
        color: GetIt.I<AppTheme>().backgroundColor,
        alignment: Alignment.centerLeft,
        child: Text(
          "RECENT CHAT",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: GetIt.I<AppTheme>().blackColor,
            fontFamily: AppFont.poppins,
            fontWeight: FontWeight.w500,
            fontSize: 14.0.toPoints(),
          ),
        ),
      ),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, i) => TextBubble(isYou: i % 2 == 0),
          childCount: 5,
        ),
      ),
    );
  }

//======================= CHAT INPUT SECTION ==================
  Widget chatInputView() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
      alignment: Alignment.center,
      child: Row(
        children: [
          Image.asset(
            AssetPath.attach,
            width: 40,
            height: 40,
          ),
          const SizedBox(
            width: 32,
          ),
          Expanded(
            child: TextField(
              minLines: 1,
              maxLines: 6,
              style: TextStyle(
                color: GetIt.I<AppTheme>().primaryTextColor,
                fontFamily: AppFont.poppins,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
              cursorColor: GetIt.I<AppTheme>().primaryTextColor,
              cursorHeight: 16,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: Strings.sendMessage,
                hintStyle: TextStyle(
                  color: GetIt.I<AppTheme>().borderGrayColor,
                  fontFamily: AppFont.poppins,
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 32,
          ),
          Row(
            children: [
              InkWell(
                child: Container(
                  alignment: Alignment.center,
                  width: 40,
                  height: 40,
                  child: Image.asset(
                    AssetPath.sendMessage,
                    width: 20,
                    height: 20,
                  ),
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              Image.asset(
                AssetPath.recordAudio,
                width: 50,
                height: 50,
              ),
            ],
          )
        ],
      ),
    );
  }
}
