import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';
import 'package:x_circle_web/utilities/extensions.dart';

class TextBubble extends StatelessWidget {
  final bool isYou;
  const TextBubble({super.key, required this.isYou});

  @override
  Widget build(BuildContext context) {
    return isYou ? rightBubble() : leftBubble();
  }

  //--------------- LEFT BUBBLE ------------------
  Widget leftBubble() {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Image.asset(
                AssetPath.story4,
                height: 42,
                width: 42,
              ),
              const SizedBox(
                width: 8,
              ),
              ConstrainedBox(
                constraints:
                    BoxConstraints(maxWidth: constraints.maxWidth * 0.6),
                child: Container(
                  margin: const EdgeInsets.only(bottom: 8),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  decoration: BoxDecoration(
                    color: GetIt.I<AppTheme>().borderColor,
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(21.0),
                      bottomRight: Radius.circular(21.0),
                      topLeft: Radius.circular(21.0),
                    ),
                  ),
                  child: Text(
                    "Are you still travelling?",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: GetIt.I<AppTheme>().blackColor,
                      fontFamily: AppFont.poppins,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0.toPoints(),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  //--------------- RIGHT BUBBLE ------------------
  Widget rightBubble() {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ConstrainedBox(
                constraints:
                    BoxConstraints(maxWidth: constraints.maxWidth * 0.6),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  decoration: BoxDecoration(
                    color: GetIt.I<AppTheme>().themeColor.withAlpha(30),
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(21.0),
                      topLeft: Radius.circular(21.0),
                      bottomLeft: Radius.circular(21.0),
                    ),
                  ),
                  child: Text(
                    "Are you still travelling?",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: GetIt.I<AppTheme>().themeColor,
                      fontFamily: AppFont.poppins,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0.toPoints(),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
