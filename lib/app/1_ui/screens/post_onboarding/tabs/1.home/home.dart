import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/chat/chat.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/threads/threads.dart';
import 'package:x_circle_web/constants/app_theme.dart';

class Home extends StatelessWidget {
  static const route = "home";

  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GetIt.I<AppTheme>().backgroundColor,
      body: Row(
        children: [
          const Threads(),
          Container(
            width: 1,
            height: double.infinity,
            color: GetIt.I<AppTheme>().borderColor,
          ),
          const Expanded(child: Chat()),
          // Container(
          //   width: 1,
          //   height: double.infinity,
          //   color: GetIt.I<AppTheme>().borderColor,
          // ),
          // const Threads(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
