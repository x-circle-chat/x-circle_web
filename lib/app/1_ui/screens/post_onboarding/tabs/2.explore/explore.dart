import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/constants/app_theme.dart';

class Explore extends StatelessWidget {
  static const route = "explore";

  const Explore({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GetIt.I<AppTheme>().backgroundColor,
      body: const Text(route),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
