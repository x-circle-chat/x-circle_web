import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import 'package:x_circle_web/app/1_ui/enums/menu_options.dart';
import 'package:x_circle_web/constants/app_theme.dart';
import 'package:x_circle_web/constants/asset_path.dart';
import 'package:x_circle_web/constants/font.dart';

class MenuTabs extends StatefulWidget {
  static const route = "menu_tabs";

  const MenuTabs({super.key});

  @override
  State<MenuTabs> createState() => _MenuTabsState();
}

class _MenuTabsState extends State<MenuTabs> {
  var currentTab = MenuOptions.home;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GetIt.I<AppTheme>().backgroundColor,
      body: Row(
        children: [
          Container(
            color: GetIt.I<AppTheme>().cardBackgroundColor,
            width: 180,
            height: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 32),
            child: Column(
              children: [
                Image.asset(
                  AssetPath.appLogo,
                  width: 80,
                  height: 80,
                ),
                const SizedBox(
                  height: 80,
                ),
                Expanded(
                    child: ListView(
                  children: CurrentData.allOptions()
                      .map((menuOption) => menuTitle(menuOption))
                      .toList(),
                ))
              ],
            ),
          ),
          Expanded(
            child: currentTab.containerWidget(),
          )
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  //============== MenuTile ====================
  Widget menuTitle(MenuOptions menuOption) {
    return Container(
      height: 60,
      padding: const EdgeInsets.only(left: 40),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 24,
              width: 24,
              child: SvgPicture.asset(
                menuOption.optionModel().icon,
                colorFilter: ColorFilter.mode(
                  menuOption == currentTab
                      ? GetIt.I<AppTheme>().themeColor
                      : GetIt.I<AppTheme>().iconColor,
                  BlendMode.srcIn,
                ),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                menuOption.optionModel().title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: GetIt.I<AppTheme>().lightTextColor,
                  fontFamily: AppFont.poppins,
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  //============================================
}
