import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_event.dart';
import 'package:x_circle_web/app/2_bloc/login_bloc/login_state.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/login_repo.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/app_exception.dart';
import 'package:x_circle_web/constants/strings.dart';
import 'package:x_circle_web/utilities/utllity.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository loginRepository;

  LoginBloc({
    required this.loginRepository,
  }) : super(LoginState.initial()) {
    on<UpdateEmail>(_mapUpdateEmailEventToState);
    on<ValidateForm>(_mapValidateFormEventToState);
    on<LoginUser>(_mapLoginUserEventToState);
    on<DismissError>(_mapErrorDismissEventToState);
  }

  void _mapUpdateEmailEventToState(
      UpdateEmail event, Emitter<LoginState> emit) async {
    emit(state.copyWith(email: event.email));
  }

  void _mapValidateFormEventToState(
      ValidateForm event, Emitter<LoginState> emit) async {
    if (state.email != "") {
      if (Utility.isValidateEmail(state.email)) {
        emit(state.copyWith(status: FormValidated()));
      } else {
        emit(state.copyWith(
            status: Error(errorMessage: Strings.validEmail, visible: true)));
      }
    } else {
      emit(state.copyWith(
          status:
              Error(errorMessage: Strings.pleaseEnterEmail, visible: true)));
    }
  }

  void _mapLoginUserEventToState(
      LoginUser event, Emitter<LoginState> emit) async {
    emit(state.copyWith(status: Loading()));
    try {
      final verificationKey =
          await loginRepository.login({"email": state.email});
      emit(state.copyWith(status: Success(verificationKey: verificationKey)));
    } catch (error) {
      debugPrint(error.toString());
      if (error is AppException) {
        emit(state.copyWith(
            status: Error(errorMessage: error.message, visible: true)));
      } else {
        emit(state.copyWith(
            status: Error(errorMessage: error.toString(), visible: true)));
      }
    }
  }

  void _mapErrorDismissEventToState(
      DismissError event, Emitter<LoginState> emit) async {
    emit(state.copyWith(
        status: Error(errorMessage: event.errorMessage, visible: false)));
  }
}
