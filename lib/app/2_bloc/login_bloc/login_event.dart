import 'package:equatable/equatable.dart';

class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object?> get props => [];
}

class UpdateEmail extends LoginEvent {
  final String email;

  const UpdateEmail({required this.email});

  @override
  List<Object?> get props => [email];
}

class ValidateForm extends LoginEvent {
  @override
  List<Object?> get props => [];
}

class LoginUser extends LoginEvent {
  @override
  List<Object?> get props => [];
}

class DismissError extends LoginEvent {
  final String errorMessage;

  const DismissError({required this.errorMessage});

  @override
  List<Object?> get props => [errorMessage];
}
