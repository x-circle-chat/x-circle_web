import 'package:equatable/equatable.dart';
import 'package:x_circle_web/app/3_data_provider/2.models/verification_key.model.dart';

//MARK: LOGIN STATUS
abstract class LoginStatus {
  List<Object?> get props => [];
}

class Initial extends LoginStatus {
  @override
  List<Object?> get props => [];
}

class FormValidated extends LoginStatus {
  @override
  List<Object?> get props => [];
}

class Loading extends LoginStatus {
  @override
  List<Object?> get props => [];
}

class Error extends LoginStatus {
  final bool visible;
  final String errorMessage;
  Error({required this.errorMessage, required this.visible});

  @override
  List<Object?> get props => [errorMessage, visible];
}

class Success extends LoginStatus {
  final VerificationKey verificationKey;

  Success({required this.verificationKey});

  @override
  List<Object?> get props => [verificationKey];
}

//MARK: LOGIN STATE
class LoginState extends Equatable {
  final LoginStatus status;
  final String email;

  const LoginState({required this.status, required this.email});

  static LoginState initial() => LoginState(status: Initial(), email: "");

  LoginState copyWith({
    LoginStatus? status,
    String? email,
  }) =>
      LoginState(
        status: status ?? this.status,
        email: email ?? this.email,
      );

  @override
  List<Object?> get props => [status, email];
}
