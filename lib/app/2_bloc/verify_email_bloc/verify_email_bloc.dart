import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_event.dart';
import 'package:x_circle_web/app/2_bloc/verify_email_bloc/verify_email_state.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/verify_email_repo.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/app_exception.dart';
import 'package:x_circle_web/constants/strings.dart';

class VerifyEmailBloc extends Bloc<VerifyEmailEvent, VerifyEmailState> {
  final VerifyEmailRepository verifyRepository;

  VerifyEmailBloc({
    required this.verifyRepository,
  }) : super(VerifyEmailState.initial()) {
    on<UpdateOtp>(_mapUpdateOtpEventToState);
    on<ValidateForm>(_mapValidateFormEventToState);
    on<VerifyUser>(_mapVerifyEmailEventToState);
    on<DismissError>(_mapDismissErrorEventToState);
  }

  void _mapUpdateOtpEventToState(
      UpdateOtp event, Emitter<VerifyEmailState> emit) async {
    emit(state.copyWith(otp: event.otp));
  }

  void _mapValidateFormEventToState(
      ValidateForm event, Emitter<VerifyEmailState> emit) async {
    final otp = state.otp ?? "";
    if (otp != "") {
      if (otp.length == 6) {
        emit(state.copyWith(status: FormValidated()));
      } else {
        emit(state.copyWith(
            status: Error(errorMessage: Strings.validOtp, visible: true)));
      }
    } else {
      emit(state.copyWith(
          status: Error(errorMessage: Strings.pleaseEnterOtp, visible: true)));
    }
  }

  void _mapVerifyEmailEventToState(
      VerifyUser event, Emitter<VerifyEmailState> emit) async {
    emit(state.copyWith(status: Loading()));
    try {
      final authModel = await verifyRepository
          .verifyEmail({"otp_id": event.otpId, "otp": state.otp});
      emit(state.copyWith(status: Success(authModel: authModel)));
    } catch (error) {
      debugPrint(error.toString());
      if (error is AppException) {
        emit(state.copyWith(
            status: Error(errorMessage: error.message, visible: true)));
      } else {
        emit(state.copyWith(
            status: Error(errorMessage: error.toString(), visible: true)));
      }
    }
  }

  void _mapDismissErrorEventToState(
      DismissError event, Emitter<VerifyEmailState> emit) async {
    emit(state.copyWith(
        status: Error(errorMessage: event.errorMessage, visible: false)));
  }
}
