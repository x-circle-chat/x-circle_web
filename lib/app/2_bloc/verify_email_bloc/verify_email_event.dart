import 'package:equatable/equatable.dart';

class VerifyEmailEvent extends Equatable {
  const VerifyEmailEvent();

  @override
  List<Object?> get props => [];
}

class UpdateOtp extends VerifyEmailEvent {
  final String otp;

  const UpdateOtp({required this.otp});

  @override
  List<Object?> get props => [otp];
}

class ValidateForm extends VerifyEmailEvent {
  @override
  List<Object?> get props => [];
}

class VerifyUser extends VerifyEmailEvent {
  final String otpId;

  const VerifyUser({required this.otpId});

  @override
  List<Object?> get props => [otpId];
}

class DismissError extends VerifyEmailEvent {
  final String errorMessage;

  const DismissError({required this.errorMessage});

  @override
  List<Object?> get props => [errorMessage];
}
