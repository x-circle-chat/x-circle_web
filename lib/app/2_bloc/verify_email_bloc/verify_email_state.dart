import 'package:equatable/equatable.dart';
import 'package:x_circle_web/app/3_data_provider/2.models/auth.model.dart';

//MARK: LOGIN STATUS
abstract class VerifyEmailStatus {
  List<Object?> get props => [];
}

class Initial extends VerifyEmailStatus {
  @override
  List<Object?> get props => [];
}

class FormValidated extends VerifyEmailStatus {
  @override
  List<Object?> get props => [];
}

class Loading extends VerifyEmailStatus {
  @override
  List<Object?> get props => [];
}

class Error extends VerifyEmailStatus {
  final bool visible;
  final String errorMessage;
  Error({required this.errorMessage, required this.visible});

  @override
  List<Object?> get props => [errorMessage, visible];
}

class Success extends VerifyEmailStatus {
  final AuthModel authModel;

  Success({required this.authModel});

  @override
  List<Object?> get props => [authModel];
}

//MARK: LOGIN STATE
class VerifyEmailState extends Equatable {
  final VerifyEmailStatus status;
  final String otp;

  const VerifyEmailState({required this.status, required this.otp});

  static VerifyEmailState initial() =>
      VerifyEmailState(status: Initial(), otp: "");

  VerifyEmailState copyWith({
    VerifyEmailStatus? status,
    String? otp,
  }) =>
      VerifyEmailState(
        status: status ?? this.status,
        otp: otp ?? this.otp,
      );

  @override
  List<Object?> get props => [status, otp];
}
