import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_event.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_state.dart';
import 'package:x_circle_web/app/3_data_provider/1.repository/auth_repo.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;

  AuthBloc({
    required this.authRepository,
  }) : super(AuthState.initial()) {
    on<Initialize>(_mapInitializeEventToState);
    on<Login>(_mapLoginEventToState);
    on<Logout>(_mapLogoutEventToState);
  }

  void _mapInitializeEventToState(
      Initialize event, Emitter<AuthState> emit) async {
    final user = authRepository.isLoggedIn();
    if (user != null) {
      if (user.isComplete == 1) {
        emit(state.copyWith(status: LoggedIn(user: user)));
      } else {
        emit(state.copyWith(status: InCompleteLoggedIn(user: user)));
      }
    } else {
      emit(state.copyWith(status: LoggedOut()));
    }
  }

  void _mapLoginEventToState(Login event, Emitter<AuthState> emit) async {
    await authRepository.saveLoginData(event.authModel);
    _mapInitializeEventToState(Initialize(), emit);
  }

  void _mapLogoutEventToState(Logout event, Emitter<AuthState> emit) async {
    await authRepository.removeLoginData();
    _mapInitializeEventToState(Initialize(), emit);
  }
}
