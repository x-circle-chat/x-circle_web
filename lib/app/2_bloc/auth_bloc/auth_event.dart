import 'package:equatable/equatable.dart';
import 'package:x_circle_web/app/3_data_provider/2.models/auth.model.dart';

class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object?> get props => [];
}

class Initialize extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class Login extends AuthEvent {
  final AuthModel authModel;

  const Login({required this.authModel});

  @override
  List<Object?> get props => [authModel];
}

class Logout extends AuthEvent {
  @override
  List<Object?> get props => [];
}
