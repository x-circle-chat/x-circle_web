import 'package:equatable/equatable.dart';
import 'package:x_circle_web/app/3_data_provider/2.models/user.model.dart';

//MARK: LOGIN STATUS
abstract class AuthStatus {
  List<Object?> get props => [];
}

class LoggedOut extends AuthStatus {
  @override
  List<Object?> get props => [];
}

class InCompleteLoggedIn extends AuthStatus {
  final User user;

  InCompleteLoggedIn({required this.user});

  @override
  List<Object?> get props => [];
}

class LoggedIn extends AuthStatus {
  final User user;

  LoggedIn({required this.user});

  @override
  List<Object?> get props => [];
}

//MARK: LOGIN STATE
class AuthState extends Equatable {
  final AuthStatus status;

  const AuthState({required this.status});

  static AuthState initial() => AuthState(status: LoggedOut());

  AuthState copyWith({
    AuthStatus? status,
    String? email,
  }) =>
      AuthState(
        status: status ?? this.status,
      );

  @override
  List<Object?> get props => [status];
}
