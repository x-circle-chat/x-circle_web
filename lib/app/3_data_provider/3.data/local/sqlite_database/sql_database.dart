import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:x_circle_web/constants/strings.dart';

abstract class SqlDatabase {
  //OPEN DATABASE AND GET REFERENCE
  static Future<Database> database() async {
    final database = await openDatabase(
      join(await getDatabasesPath(), '${Strings.appName}.db'),
      version: 1,
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)',
        );
      },
    );

    //CREATE TABLES
    await _createTables(database);

    //RETURN DATABASE
    return database;
  }

  static Future _createTables(Database database) async {
    Batch batch = database.batch();

    // USERS TABLE
    batch.execute("""CREATE TABLE IF NOT EXISTS users(
    id TEXT PRIMARY KEY, 
    email TEXT, 
    name TEXT, 
    gender TEXT, 
    birthday TEXT, 
    profile_pic TEXT, 
    isComplete INTEGER, 
    isVerified INTEGER, 
    createdAt TEXT, 
    updatedAt TEXT
    )""");

    List<dynamic> result = await batch.commit();
    debugPrint(result.toString());
  }
}
