import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/local/shared_preference/preferences_key.dart';

class SharedPreferenceHelper {
  // shared pref instance
  final SharedPreferences _sharedPreference;

  // constructor
  SharedPreferenceHelper(this._sharedPreference);

  // Auth Token: ----------------------------------------------------------
  String? get authToken {
    return _sharedPreference.getString(PreferencesKey.authToken);
  }

  Future<bool> saveAuthToken(String authToken) async {
    return _sharedPreference.setString(PreferencesKey.authToken, authToken);
  }

  Future<bool> removeAuthToken() async {
    return _sharedPreference.remove(PreferencesKey.authToken);
  }
  // ---------------------------------------------------------------------

  // Auth Token: ----------------------------------------------------------
  Map<String, dynamic>? get user {
    final jsonString = _sharedPreference.getString(PreferencesKey.user) ?? "";
    return jsonDecode(jsonString);
  }

  Future<bool> saveUser(Map<String, dynamic> json) async {
    final jsonString = jsonEncode(json);
    return _sharedPreference.setString(PreferencesKey.user, jsonString);
  }

  Future<bool> removeUser() async {
    return _sharedPreference.remove(PreferencesKey.user);
  }
  // ---------------------------------------------------------------------
}
