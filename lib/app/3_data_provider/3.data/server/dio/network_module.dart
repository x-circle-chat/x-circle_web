import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/local/shared_preference/shared_preference_helper.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/dio/logging_interceptor.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/endpoints.dart';

abstract class NetworkModule {
  static Dio provideDio(SharedPreferenceHelper sharedPrefHelper) {
    final dio = Dio();

    dio
      ..options.baseUrl = Endpoints.baseUrl
      ..options.connectTimeout = Endpoints.connectionTimeout
      ..options.receiveTimeout = Endpoints.receiveTimeout
      ..options.headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
      ..interceptors.add(LoggingInterceptor())
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (RequestOptions options,
              RequestInterceptorHandler handler) async {
            // getting token
            var token = sharedPrefHelper.authToken;
            if (token != null && token.isNotEmpty) {
              options.headers
                  .putIfAbsent('Authorization', () => "Bearer $token");
              log(token);
            } else {
              log('Auth token is null');
            }
            return handler.next(options);
          },
        ),
      );

    return dio;
  }
}
