import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';

class LoggingInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    log("""REQUEST:============================================================\n
        ${(options)}
    \n=====================================================================""");
    return super.onRequest(options, handler);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    log("""ERROR:==============================================================\n
    URL: ${err.requestOptions.uri}\n
    Method: ${err.requestOptions.method} 
    Headers: ${json.encode(err.response?.headers.map)}
    StatusCode: ${err.response?.statusCode}
    Data: ${json.encode(err.response?.data)}
    <-- END HTTP
    \n=====================================================================""");
    return super.onError(err, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log("""RESPONSE:===========================================================\n
    URL: ${response.requestOptions.uri}
    Method: ${response.requestOptions.method}
    Headers: ${json.encode(response.requestOptions.headers)}
    Data: ${response.data} 
    \n======================================================================""");
    return super.onResponse(response, handler);
  }
}
