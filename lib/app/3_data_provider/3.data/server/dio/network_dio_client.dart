import 'package:dio/dio.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/api_error_messages.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/app_exception.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/dio_error_util.dart';

class NetworkClientDio {
  // dio instance
  final Dio _dio;

  // injecting dio instance
  NetworkClientDio(this._dio);

  // Get:-----------------------------------------------------------------------
  Future<dynamic> get(
    String uri, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final Response response = await _dio.get(
        uri,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      return response.data;
    } on DioException catch (e) {
      throw AppException(message: DioErrorUtil.handleError(e));
    } catch (e) {
      throw AppException(message: APIErrorsMessages.unknownError);
    }
  }

  // Post:----------------------------------------------------------------------
  Future<dynamic> post(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final Response response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response.data;
    } on DioException catch (e) {
      throw AppException(message: DioErrorUtil.handleError(e));
    } catch (e) {
      throw AppException(message: APIErrorsMessages.unknownError);
    }
  }

  // Put:-----------------------------------------------------------------------
  Future<dynamic> put(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final Response response = await _dio.put(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response.data;
    } on DioException catch (e) {
      throw AppException(message: DioErrorUtil.handleError(e));
    } catch (e) {
      throw AppException(message: APIErrorsMessages.unknownError);
    }
  }

  // Delete:--------------------------------------------------------------------
  Future<dynamic> delete(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final Response response = await _dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return response.data;
    } on DioException catch (e) {
      throw AppException(message: DioErrorUtil.handleError(e));
    } catch (e) {
      throw AppException(message: APIErrorsMessages.unknownError);
    }
  }
}
