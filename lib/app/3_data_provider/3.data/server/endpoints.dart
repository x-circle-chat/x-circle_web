class Endpoints {
  Endpoints._();

  //baseUrl
  static const String baseUrl = "http://127.0.0.1:3000";

  // receiveTimeout
  static const Duration receiveTimeout = Duration(seconds: 180);

  // connectTimeout
  static const Duration connectionTimeout = Duration(seconds: 180);

  // booking endpoints
  static const String login = "$baseUrl/login";
  static const String verify = "$baseUrl/verify";
}
