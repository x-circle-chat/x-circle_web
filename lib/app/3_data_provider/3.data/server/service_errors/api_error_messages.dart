class APIErrorsMessages {
  APIErrorsMessages._();

  static const String connectionError =
      "Please check your internet connection.";
  static const String timeoutError = "Please check your internet connection.";
  static const String unexpectedError = "Unexpected error occurred.";
  static const String unknownError = 'Oops something went wrong';
  static const String serverError = 'Internal server error';
  static const String badRequestError = 'Bad request';
  static const String unexpectedResponse = 'Unexpected response From server';
}
