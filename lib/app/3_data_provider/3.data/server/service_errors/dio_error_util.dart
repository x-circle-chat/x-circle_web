import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/api_error_messages.dart';

class DioErrorUtil {
  static String handleError(DioException error, {bool isLogin = false}) {
    String errorDescription = "";
    switch (error.type) {
      case DioExceptionType.cancel:
        errorDescription = APIErrorsMessages.timeoutError;
        break;
      case DioExceptionType.connectionTimeout:
        errorDescription = APIErrorsMessages.timeoutError;
        break;
      case DioExceptionType.connectionError:
        errorDescription = APIErrorsMessages.connectionError;
        break;
      case DioExceptionType.receiveTimeout:
        errorDescription = APIErrorsMessages.timeoutError;
        break;
      case DioExceptionType.sendTimeout:
        errorDescription = APIErrorsMessages.timeoutError;
        break;
      case DioExceptionType.badCertificate:
        errorDescription = APIErrorsMessages.badRequestError;
        break;
      case DioExceptionType.badResponse:
        var responseJson = jsonDecode(error.response.toString());
        final handleError =
            _handleError(error.response!.statusCode!, error.response!.data);
        if (responseJson != null &&
            (responseJson is String) &&
            responseJson != "") {
          errorDescription = responseJson;
        } else if (responseJson != null &&
            (responseJson is Map<String, dynamic>)) {
          errorDescription = responseJson["message"] ?? handleError;
        } else {
          errorDescription = handleError;
        }
        break;
      case DioExceptionType.unknown:
        errorDescription = APIErrorsMessages.unknownError;
        break;
    }
    return errorDescription;
  }

  static String _handleError(int statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        return APIErrorsMessages.badRequestError;
      case 404:
        return error["message"];
      case 500:
        return APIErrorsMessages.serverError;
      default:
        return APIErrorsMessages.unknownError;
    }
  }
}
