class AppException implements Exception {
  dynamic message;

  AppException({required this.message});

  @override
  String toString() {
    return "$message";
  }
}
