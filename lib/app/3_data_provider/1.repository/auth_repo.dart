import 'package:x_circle_web/app/3_data_provider/2.models/auth.model.dart';
import 'package:x_circle_web/app/3_data_provider/2.models/user.model.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/local/shared_preference/shared_preference_helper.dart';

class AuthRepository {
  // dio instance
  final SharedPreferenceHelper _sharedPrefHelper;

  // injecting dio instance
  AuthRepository(this._sharedPrefHelper);

  //Get Login Status
  User? isLoggedIn() {
    final authToken = _sharedPrefHelper.authToken;

    if (authToken != null && authToken != "") {
      Map<String, dynamic>? userJson = _sharedPrefHelper.user;

      if (userJson != null && userJson.isNotEmpty) {
        return User.fromJson(userJson);
      }
    }
    return null;
  }

  //Save Login Data
  Future<bool> saveLoginData(AuthModel authModel) async {
    final saveToken =
        await _sharedPrefHelper.saveAuthToken(authModel.authToken);
    final saveUser = await _sharedPrefHelper.saveUser(authModel.user.toJson());
    return saveToken && saveUser;
  }

  //Remove Login Data
  Future<bool> removeLoginData() async {
    final removeToken = await _sharedPrefHelper.removeAuthToken();
    final removeUser = await _sharedPrefHelper.removeUser();
    return removeToken && removeUser;
  }
}
