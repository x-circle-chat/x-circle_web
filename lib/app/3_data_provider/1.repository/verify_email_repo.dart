import 'dart:developer';

import 'package:x_circle_web/app/3_data_provider/2.models/auth.model.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/dio/network_dio_client.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/endpoints.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/api_error_messages.dart';
import 'package:x_circle_web/app/3_data_provider/3.data/server/service_errors/app_exception.dart';

class VerifyEmailRepository {
  // dio instance
  final NetworkClientDio _dioClient;

  // injecting dio instance
  VerifyEmailRepository(this._dioClient);

  //Verify
  Future<AuthModel> verifyEmail(Map<String, dynamic> requestData) async {
    try {
      final res = await _dioClient.post(Endpoints.verify, data: requestData);
      final json = res["data"];
      if (json is Map<String, dynamic>) {
        return AuthModel.fromJson(json);
      } else {
        throw AppException(message: APIErrorsMessages.unexpectedResponse);
      }
    } catch (e) {
      log(e.toString());
      throw e is AppException
          ? e
          : AppException(message: APIErrorsMessages.unknownError);
    }
  }
}
