// ignore_for_file: non_constant_identifier_names
import 'package:get_it/get_it.dart';
import 'package:sqflite/sqflite.dart';

class User {
  final String id;
  final String email;
  final String name;
  final String gender;
  final String birthday;
  final String profile_pic;
  final int isComplete;
  final int isVerified;
  final String createdAt;
  final String updatedAt;

  User._({
    required this.id,
    required this.email,
    required this.name,
    required this.gender,
    required this.birthday,
    required this.profile_pic,
    required this.isComplete,
    required this.isVerified,
    required this.createdAt,
    required this.updatedAt,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User._(
      id: json["id"] ?? "",
      email: json["email"] ?? "",
      name: json["name"] ?? "",
      gender: json["gender"] ?? "",
      birthday: json["birthday"] ?? "",
      profile_pic: json["profile_pic"] ?? "",
      isComplete: json["isComplete"] ?? 0,
      isVerified: json["isVerified"] ?? 0,
      createdAt: json["createdAt"] ?? "",
      updatedAt: json["updatedAt"] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "email": email,
      "name": name,
      "gender": gender,
      "birthday": birthday,
      "profile_pic": profile_pic,
      "isComplete": isComplete,
      "isVerified": isVerified,
      "createdAt": createdAt,
      "updatedAt": updatedAt,
    };
  }
}

extension UserTable on User {
  //INSER USER
  static Future<void> insert(User user) async {
    final db = GetIt.I<Database>();
    await db.insert(
      'users',
      user.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

//FETCH USERS
  static Future<List<User>> users({String where = ""}) async {
    // Get a reference to the database.
    final db = GetIt.I<Database>();

    // Query the table for all the dogs.
    final List<Map<String, Object?>> results = where == ""
        ? await db.query('users')
        : await db.query('users', where: where);

    // Convert the list of each dog's fields into a list of `Dog` objects.
    return results.map((json) => User.fromJson(json)).toList();
  }
}
