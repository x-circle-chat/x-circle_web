import 'package:x_circle_web/app/3_data_provider/2.models/user.model.dart';

class AuthModel {
  final String authToken;
  final User user;

  AuthModel._({
    required this.authToken,
    required this.user,
  });

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel._(
      authToken: json["token"] ?? "",
      user: User.fromJson(json),
    );
  }
}
