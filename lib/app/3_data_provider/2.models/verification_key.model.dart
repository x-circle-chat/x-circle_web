class VerificationKey {
  final String timestamp;
  final String otpId;

  VerificationKey._({required this.timestamp, required this.otpId});

  factory VerificationKey.fromJson(Map<String, dynamic> json) {
    return VerificationKey._(
      timestamp: json["timestamp"] ?? "",
      otpId: json["otp_id"] ?? "",
    );
  }

  Map<String, dynamic> toJson(VerificationKey model) {
    return {"timestamp": timestamp, "otpId": otpId};
  }
}
