import 'package:flutter/material.dart';

class AppTheme {
  final bool isLightTheme;

  AppTheme({required this.isLightTheme});

  Color get themeColor {
    return isLightTheme ? const Color(0xff24B47E) : const Color(0xff24B47E);
  }

  Color get backgroundColor {
    return isLightTheme ? const Color(0xffFFFFFF) : const Color(0xff181818);
  }

  Color get cardBackgroundColor {
    return isLightTheme ? const Color(0xfffafafa) : const Color(0xff212121);
  }

  Color get primaryTextColor {
    return isLightTheme ? const Color(0xff3A3A3A) : const Color(0xffFDFDFD);
  }

  Color get subtitleTextColor {
    return isLightTheme ? const Color(0xff9C9797) : const Color(0xff666666);
  }

  Color get lightTextColor {
    return isLightTheme ? const Color(0xff989898) : const Color(0xff989898);
  }

  Color get borderColor {
    return isLightTheme ? const Color(0xffF6F6F6) : const Color(0xff2A2A2A);
  }

  Color get borderGrayColor {
    return const Color(0xffE4E4E4);
  }

  Color get inputBGColor {
    return isLightTheme ? const Color(0xffF6F6F6) : const Color(0xff323232);
  }

  Color get inputBorderColor {
    return isLightTheme ? const Color(0xffF6F6F6) : const Color(0xff3E3E3E);
  }

  Color get blackColor {
    return isLightTheme ? const Color(0xff000000) : const Color(0xffFFFFFF);
  }

  Color get whiteColor {
    return isLightTheme ? const Color(0xffFFFFFF) : const Color(0xff000000);
  }

  Color get alwaysWhiteColor {
    return const Color(0xffFFFFFF);
  }

  Color get alwaysBlackColor {
    return const Color(0xff000000);
  }

  Color get iconColor {
    return const Color(0xffA8A8A8);
  }
}
