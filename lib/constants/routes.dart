
import 'package:go_router/go_router.dart';
import 'package:x_circle_web/app/1_ui/screens/onboarding/complete_profile.dart';
import 'package:x_circle_web/app/1_ui/screens/onboarding/login/login.dart';
import 'package:x_circle_web/app/1_ui/screens/onboarding/login/verify_email.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/1.home/home.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/2.explore/explore.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/3.feed/feed.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/4.settings/settings.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/5.contact/contact.dart';
import 'package:x_circle_web/app/1_ui/screens/post_onboarding/tabs/menu_tabs.dart';
import 'package:x_circle_web/app/2_bloc/auth_bloc/auth_state.dart';

// GoRouter configuration
GoRouter routerConfig(AuthStatus authStatus) {
  final initialLocation = (authStatus is LoggedIn)
      ? "/${MenuTabs.route}"
      : ((authStatus is InCompleteLoggedIn)
          ? "/${CompleteProfile.route}"
          : "/");
  return GoRouter(
    initialLocation: initialLocation,
    routes: [
      GoRoute(
        name: Login.route,
        path: '/',
        builder: (context, state) => Login(),
      ),
      GoRoute(
        name: VerifyEmail.route,
        path: "/${VerifyEmail.route}/extra",
        builder: (context, state) => VerifyEmail(
          email: (state.extra as Map)["email"] ?? "",
          otpId: (state.extra as Map)["otpId"] ?? "",
        ),
      ),
      GoRoute(
        name: CompleteProfile.route,
        path: "/${CompleteProfile.route}",
        builder: (context, state) => CompleteProfile(),
      ),
      GoRoute(
        name: MenuTabs.route,
        path: "/${MenuTabs.route}",
        builder: (context, state) => const MenuTabs(),
      ),
      GoRoute(
        name: Home.route,
        path: "/${Home.route}",
        builder: (context, state) => const Home(),
      ),
      GoRoute(
        name: Explore.route,
        path: "/${Explore.route}",
        builder: (context, state) => const Explore(),
      ),
      GoRoute(
        name: Feed.route,
        path: "/${Feed.route}",
        builder: (context, state) => const Feed(),
      ),
      GoRoute(
        name: Settings.route,
        path: "/${Settings.route}",
        builder: (context, state) => const Settings(),
      ),
      GoRoute(
        name: Contact.route,
        path: "/${Contact.route}",
        builder: (context, state) => const Contact(),
      ),
    ],
  );
}
