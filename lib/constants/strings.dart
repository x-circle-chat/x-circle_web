
class Strings {
  static const appName = "X-CIRCLE";

  //GENERAL
  static const okay = "Okay";
  static const error = "Error!";
  static const alert = "Alert!";

//MENU
  static const home = "Home";
  static const explore = "Explore";
  static const feed = "Feed";
  static const settings = "Settings";
  static const contact = "Contact";

  //Login
  static const loginButton = "Login";
  static const enterEmailHeadline = "Please enter your email address for login";
  static const enterEmail = "Enter email address";

  static const completeYourProfile = "Please complete your profile";
  static const enterName = "Enter your name";
  static const selGender = "Select your gender";
  static const enterBirthday = "Enter your birthday";

  //Login
  static const submitButton = "Submit";
  static const enterOTPHeadline = "Please enter OTP sent to ";

  //Home/Chat
  static const chat = "Chat";
  static const searchHere = "Search here...";

  static const message = "Message";
  static const archive = "Archive Chat";

  static const sendMessage = "Send Message";

  //Validation Messages
  static const pleaseEnterEmail = "Email address can not be empty.";
  static const validEmail = "Please enter valid email address.";

  static const pleaseEnterOtp = "Otp can not be empty.";
  static const validOtp = "Please enter valid otp.";
}
