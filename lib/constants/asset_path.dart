class AssetPath {
  static const assetsPath = "lib/assets/";
  static const imagePath = "${assetsPath}images/";
  static const imageTempPath = "${assetsPath}images/temp/";

  //Images and Icons
  static const appLogo = "${imagePath}app_logo.png";

  //Menu Icons
  static const homeLogo = "${imagePath}home.svg";
  static const exploreLogo = "${imagePath}explore.svg";
  static const feedLogo = "${imagePath}feed.svg";
  static const settingsLogo = "${imagePath}settings.svg";
  static const contactLogo = "${imagePath}contact.svg";

  //Home Icons
  static const search = "${imagePath}search.png";
  static const record = "${imagePath}record.png";

  static const add = "${imagePath}add.png";

  static const camera = "${imagePath}camera.png";
  static const phone = "${imagePath}phone.png";

  static const attach = "${imagePath}attach.png";
  static const recordAudio = "${imagePath}record_audio.png";
  static const sendMessage = "${imagePath}send_message.png";

  //************ TEMP ICON *******************/
  static const story1 = "${imageTempPath}story1.png";
  static const story2 = "${imageTempPath}story2.png";
  static const story3 = "${imageTempPath}story3.png";
  static const story4 = "${imageTempPath}story4.png";
  static const story5 = "${imageTempPath}story5.png";
  //******************************************/
}

//Lottie Paths
extension LottiePaths on AssetPath {
  static const loader = "${AssetPath.assetsPath}files/lottie_files/loader.json";
  static const loader1 =
      "${AssetPath.assetsPath}files/lottie_files/loader1.json";
}
